package vapor.rts

import com.badlogic.gdx.Input.Buttons
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType
import com.badlogic.gdx.graphics.{Color, GL20, OrthographicCamera}
import com.badlogic.gdx.graphics.glutils.ShapeRenderer

import scala.collection.mutable

import com.badlogic.gdx.{InputProcessor, Gdx, Screen}

case class SmoothedPath(point: Point, path: mutable.Buffer[Point])

class SmoothingProto extends Screen {
    private val _shapeRenderer = new ShapeRenderer()
    private val _camera = new OrthographicCamera(Gdx.graphics.getWidth, Gdx.graphics.getHeight)
    private val _paths = mutable.Buffer[SmoothedPath]()
    private var _smoothIdx = 0
    private var _totalSmooths = 0

    val map = new Map(256, 256)

    {
        val path = mutable.Buffer[Point]()
        path += Point(0, 0)
        path += Point(0, 1)
        path += Point(0, 2)
        path += Point(1, 3)
        path += Point(1, 4)
        path += Point(1, 5)
        path += Point(1, 6)
        path += Point(2, 6)
        path += Point(2, 5)
        path += Point(2, 4)
        path += Point(3, 3)
        path += Point(3, 2)
        path += Point(3, 1)
        path += Point(3, 0)
        _paths += SmoothedPath(path(0), path)

        Gdx.input.setInputProcessor(new InputProcessor {
            override def keyTyped(character: Char): Boolean = true
            override def mouseMoved(screenX: Int, screenY: Int): Boolean = true
            override def keyDown(keycode: Int): Boolean = true
            override def touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = {
                button match {
                    case Buttons.LEFT =>
                        val lastPath = _paths.last
                        val clone = mutable.Buffer[Point]()
                        clone ++= lastPath.path
                        val idx = _smoothIdx
                        smooth(clone, _ => true)
                        if(_paths.length < 60) {
                            _paths += SmoothedPath(clone(idx), clone)
                        }
                        else {
                            _paths(_totalSmooths % 60) = SmoothedPath(clone(idx), clone)
                        }
                }
                true
            }
            override def keyUp(keycode: Int): Boolean = true
            override def scrolled(amount: Int): Boolean = true
            override def touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean = true
            override def touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean = true
        })
    }

    override def render(delta: Float): Unit = {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
        Gdx.gl.glLineWidth(2)

        _camera.update()
        _shapeRenderer.setProjectionMatrix(_camera.combined)

        _shapeRenderer.begin(ShapeType.Line)
        for((smoothedPath, pathIdx) <- _paths.zipWithIndex) {
            val r = 1//1 - ((pathIdx * .2f) % 1)
            _shapeRenderer.setColor(new Color(r, 0, 0, 1))
            val xOffset = (pathIdx % 15) * 128
            val yOffset = (pathIdx / 15) * 256
            var pointIdx = 0
            val path = smoothedPath.path
            while(pointIdx < path.length - 1) {
                val tile1 = path(pointIdx)
                val tile2 = path(pointIdx + 1)
                _shapeRenderer.line(
                    tile1.pixelX + 16 + xOffset,
                    tile1.pixelY + 16 + yOffset,
                    tile2.pixelX + 16 + xOffset,
                    tile2.pixelY + 16 + yOffset
                )
                pointIdx += 1
            }
        }
        _shapeRenderer.end()

        _shapeRenderer.begin(ShapeType.Filled)
        _shapeRenderer.setColor(Color.CYAN)
        for((smoothedPath, pathIdx) <- _paths.zipWithIndex) {
            val xOffset = (pathIdx % 15) * 128
            val yOffset = (pathIdx / 15) * 256
            val point = smoothedPath.point
            _shapeRenderer.circle(point.pixelX + 16 + xOffset, point.pixelY + 16 + yOffset, 4)
        }
        _shapeRenderer.end()
    }

    override def hide(): Unit = {

    }

    override def resize(width: Int, height: Int): Unit = {
        _camera.setToOrtho(false, width, height)
    }

    override def dispose(): Unit = {

    }

    override def pause(): Unit = {

    }

    override def show(): Unit = {

    }

    override def resume(): Unit = {

    }

    private def validDiagonal(center: Point, t: Point, isValid: Point => Boolean) = {
        if(t.x != center.x && t.y != center.y) {
            isValid(t) && isValid(map.getTile(t.x, center.y).get) && isValid(map.getTile(center.x, t.y).get)
        }
        else {
            isValid(t)
        }
    }

    private def smooth(tilePath: mutable.Buffer[Point], isValid: Point => Boolean): Unit = {
        val current = tilePath(_smoothIdx)
        val next = tilePath(_smoothIdx + 1)
        val next2 = tilePath(_smoothIdx + 2)
        val currXDir = next.x - current.x
        val currYDir = next.y - current.y
        val newXDir = next2.x - next.x
        val newYDir = next2.y - next.y
        if (currXDir != newXDir || currYDir != newYDir) {
            val xDiff = next2.x - current.x
            val yDiff = next2.y - current.y
            val xDir = if (xDiff > 0) 1 else if (xDiff < 0) -1 else 0
            val yDir = if (yDiff > 0) 1 else if (yDiff < 0) -1 else 0
            val point1 =
                if (currXDir == 0 || currYDir == 0) {
                    (xDir, yDir)
                }
                else {
                    (xDir, 0)
                }
            val p1Dist = Util.distance2(point1._1, point1._2, xDiff, yDiff)
            val point2 =
                if (currXDir == 0) {
                    (-xDir, yDir)
                }
                else if (currYDir == 0) {
                    (xDir, -yDir)
                }
                else {
                    (0, yDir)
                }
            val p2Dist = Util.distance2(point2._1, point2._2, xDiff, yDiff)
            val point = if (p1Dist < p2Dist) point1 else point2

            val maybeCloser = map.getTile(current.x + point._1, current.y + point._2).get
            if (maybeCloser == next2 && validDiagonal(current, maybeCloser, isValid)) {
                tilePath.remove(_smoothIdx + 1)
                //_smoothIdx -= 1
            }
            else if (validDiagonal(current, maybeCloser, isValid) && validDiagonal(maybeCloser, next2, isValid)) {
                tilePath(_smoothIdx + 1) = maybeCloser
            }
        }

        if(_smoothIdx < tilePath.length - 3) {
            _smoothIdx += 1
        }
        else {
            _smoothIdx = 0
        }

        _totalSmooths += 1
    }
}
