package vapor.rts

case class Point(x: Int, y: Int) {
    val pixelX = x * 32
    val pixelY = y * 32
}

class Map(width: Int, height: Int) {
    private val _tiles = scala.Array.ofDim[Point](width, height)

    for {
        i <- 0 until width
        j <- 0 until height
    } {
        _tiles(i)(j) = Point(i, j)
    }

    def inBounds(x: Int, y: Int) = x >= 0 && y >= 0 && x < width && y < height

    def getTile(tileX: Int, tileY: Int): Option[Point] = {
        if(inBounds(tileX, tileY)) {
            Some(_tiles(tileX)(tileY))
        }
        else {
            None
        }
    }
}
