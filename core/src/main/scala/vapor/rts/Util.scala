package vapor.rts

object Util {
    def distance2(x1: Float, y1: Float, x2: Float, y2: Float): Double = {
        val dx = x1 - x2
        val dy = y1 - y2
        dx * dx + dy * dy
    }

    def distance(x1: Float, y1: Float, x2: Float, y2: Float): Double = {
        math.sqrt(distance2(x1, y1, x2, y2))
    }
}
