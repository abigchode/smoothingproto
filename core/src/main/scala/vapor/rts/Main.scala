package vapor.rts

import com.badlogic.gdx.Game

object Main extends Game {
    private var _game: SmoothingProto = null

    override def create(): Unit = {
        _game = new SmoothingProto()
        setScreen(_game)
    }
}
