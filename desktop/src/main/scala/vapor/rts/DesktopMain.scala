package vapor.rts

import com.badlogic.gdx.backends.lwjgl._

object DesktopMain extends App {
    val cfg = new LwjglApplicationConfiguration
    cfg.title = "smoothing_proto"
    cfg.height = 1080
    cfg.width = 1920
    cfg.forceExit = false
    new LwjglApplication(Main, cfg)
}
